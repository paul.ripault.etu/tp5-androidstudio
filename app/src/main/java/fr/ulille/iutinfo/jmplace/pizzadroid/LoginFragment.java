package fr.ulille.iutinfo.jmplace.pizzadroid;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import fr.ulille.iutinfo.jmplace.pizzadroid.pizzaland.Pizza;

public class LoginFragment extends Fragment {
    private PizzaViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(PizzaViewModel.class);


        view.findViewById(R.id.button_first).setOnClickListener(view1 -> {
            EditText etLogin = getActivity().findViewById(R.id.etLogin);
            EditText etPasswd = getActivity().findViewById(R.id.etPasswd);
            if (etPasswd.getText().toString().equals("Doe")) {
                model.login(etLogin.getText().toString());
                NavHostFragment.findNavController(LoginFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            } else {
                Toast failed = Toast.makeText(getActivity(), R.string.failed_message, Toast.LENGTH_LONG);
                failed.show();
                etLogin.setText("");
                etPasswd.setText("");
                model.logout();
            }
        });
}
}