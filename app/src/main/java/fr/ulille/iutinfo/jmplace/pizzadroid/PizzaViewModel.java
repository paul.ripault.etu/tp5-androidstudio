package fr.ulille.iutinfo.jmplace.pizzadroid;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import fr.ulille.iutinfo.jmplace.pizzadroid.pizzaland.Panier;
import fr.ulille.iutinfo.jmplace.pizzadroid.pizzaland.Pizza;

public class PizzaViewModel extends ViewModel {
    private String loggedUser;
    private Pizza[] pizzas;
    private Panier panier;
    private MutableLiveData<Integer> selected;


    public PizzaViewModel() {
        this.loggedUser = null;
        this.panier = null;
        this.selected = null;
    }

    public void setPizzas(Pizza[] pizzas) {
        this.pizzas = pizzas;
    }

    public String getLoggedUser() {
        return this.loggedUser;
    }

    public void login(String loggedUser) {
        this.loggedUser = loggedUser;
        this.panier = new Panier(new Pizza[pizzas.length]);
    }

    public void logout() {
        login(null);
    }

    public Panier getPanier() {
        return this.panier;
    }

    public Pizza[] getPizzas() {
        return this.pizzas;
    }

    public Integer getSelected() {
        return getLiveSelected().getValue();
    }
    public void setSelected(Integer selected) {
        getLiveSelected().setValue(selected);
    }

    private MutableLiveData<Integer> getLiveSelected() {
        if (selected == null) {
            selected = new MutableLiveData<>();
        }
        return selected;
    }
    @MainThread
    public void observeSelection(@NonNull LifecycleOwner owner, @NonNull Observer<? super Integer> observer) {
        getLiveSelected().observe(owner, observer);
    }
}
