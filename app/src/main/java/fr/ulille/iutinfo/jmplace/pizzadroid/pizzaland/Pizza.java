package fr.ulille.iutinfo.jmplace.pizzadroid.pizzaland;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import fr.ulille.iutinfo.jmplace.pizzadroid.R;

public class Pizza {
    private static  Pizza[] pizzas;
    private final String id;
    private final String nom;
    private final String base;
    private final double prix_petite;
    private final double prix_grande;
    private String priceSmall;
    private String priceLarge;

    public static Pizza[] loadPizzas(Context context) {
        pizzas = loadPizzasFromResources(context, R.raw.pizzas);
        Pizza.formatPrice(context);
        return pizzas;
    }
    public static Pizza[] loadPizzasFromResources(Context context, int resId)  {
        StringBuilder text = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(resId)));
        String line;
        try {
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
                br.close();
            JSONArray array = new JSONArray(text.toString());
            pizzas = LoadPizzasFromJson(array);
        } catch (JSONException | IOException e) {
            Toast failedLoad = Toast.makeText(context, R.string.failedLoad, Toast.LENGTH_LONG);
            failedLoad.show();
        }
        return pizzas;
    }
    private static Pizza[] LoadPizzasFromJson(JSONArray json) throws JSONException {
        Pizza[] pizzas = new Pizza[json.length()];
        for (int i = 0 ; i < json.length(); i++) {
            pizzas[i] = new Pizza(json.getJSONObject(i));
        }
        return pizzas;
    }

    public static void formatPrice(Context context) {
        for (Pizza p : pizzas) {
            p.priceSmall = context.getResources().getString(R.string.price, p.prix_petite);
            p.priceLarge = context.getResources().getString(R.string.price, p.prix_grande);
        }
    }

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getBase() {
        return base;
    }

    public double getPrix_petite() {
        return prix_petite;
    }

    public double getPrix_grande() {
        return prix_grande;
    }

    public String getPriceSmall() {
        return priceSmall;
    }

    public String getPriceLarge() {
        return priceLarge;
    }

    private Pizza(JSONObject json) throws JSONException {
        this.id = json.getString("id");
        this.nom = json.getString("nom");
        this.base = json.getString("base");
        this.prix_petite = json.getDouble("prix_petite");
        this.prix_grande = json.getDouble("prix_grande");
    }
}
