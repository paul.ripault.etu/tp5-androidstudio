package fr.ulille.iutinfo.jmplace.pizzadroid;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import fr.ulille.iutinfo.jmplace.pizzadroid.pizzaland.Pizza;

public class LoggedFragment extends Fragment implements Observer<Integer> {
    private PizzaViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_logged, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        model = new ViewModelProvider(requireActivity()).get(PizzaViewModel.class);

        TextView welcome = getActivity().findViewById(R.id.welcome);
        String welcomeMsg = getActivity().getResources().getString(R.string.hello, model.getLoggedUser());
        welcome.setText(welcomeMsg);

        view.findViewById(R.id.button_second).setOnClickListener(view1 -> NavHostFragment.findNavController(LoggedFragment.this)
                .navigate(R.id.action_SecondFragment_to_FirstFragment));

        // RecyclerView Setup
        RecyclerView pizzasList = getActivity().findViewById(R.id.pizzasList);
        pizzasList.setLayoutManager(new LinearLayoutManager(getActivity()));
        PizzaAdapter adapter = new PizzaAdapter(model);
        pizzasList.setAdapter(adapter);
        update();
        model.observeSelection(this, this);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void update() {
        TextView tvMontant = getActivity().findViewById(R.id.etAmount);
        tvMontant.setText(getActivity().getResources().getString(R.string.price, model.getPanier().getAmount()));
    }

    @Override
    public void onChanged(Integer integer) {
        Log.d("PIZZA", "update logged fragment");
    }
}