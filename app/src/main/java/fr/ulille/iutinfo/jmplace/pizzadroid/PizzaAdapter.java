package fr.ulille.iutinfo.jmplace.pizzadroid;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import fr.ulille.iutinfo.jmplace.pizzadroid.pizzaland.Pizza;


public class PizzaAdapter extends RecyclerView.Adapter<PizzaAdapter.ViewHolder> {
    final PizzaViewModel model;

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;



        public void setName(String name) {
            ((TextView) itemView.findViewById(R.id.name)).setText(name);
        }

        public void setBase(String base) {
            ((TextView) itemView.findViewById(R.id.base)).setText(base);
        }
        public void setPriceSmall(double price) {
            String formatted = itemView.getContext().getResources().getString(R.string.price, price);
            ((TextView) itemView.findViewById(R.id.price_small)).setText(formatted);
        }

        public void setPriceLarge(double price) {
            String formatted = itemView.getContext().getResources().getString(R.string.price, price);
            ((TextView) itemView.findViewById(R.id.price_large)).setText(formatted);
        }

        public void setQtySmall(int qty) {
            ((TextView) itemView.findViewById(R.id.qty_small)).setText("" + qty);
        }

        public void setQtyLarge(int qty) {
            ((TextView) itemView.findViewById(R.id.qty_large)).setText("" + qty);
        }

        public TextView getNameView() {
            return itemView.findViewById(R.id.name);
        }

        public TextView getBaseView() {
            return itemView.findViewById(R.id.base);
        }

        public TextView getPriceSmallView() {
            return itemView.findViewById(R.id.price_small);
        }

        public TextView getPriceLargeView() {
            return itemView.findViewById(R.id.price_large);
        }

        public TextView getQtySmallView() {
            return itemView.findViewById(R.id.qty_small);
        }

        public TextView getQtyLargeView() {
            return itemView.findViewById(R.id.qty_large);
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.itemView.setOnClickListener(view -> {
                select(getAdapterPosition());
            });
        }
    }

    public PizzaAdapter(PizzaViewModel model) {
        this.model = model;
    }

    @NonNull
    @Override
    public PizzaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pizza_view_holder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaAdapter.ViewHolder holder, int position) {
        Pizza pizza = model.getPizzas()[position];
        holder.getNameView().setText(pizza.getNom());
        holder.getBaseView().setText(pizza.getBase());
        holder.getPriceSmallView().setText("" + pizza.getPriceSmall());
        holder.getPriceLargeView().setText("" + pizza.getPriceLarge());
        holder.getQtySmallView().setText("" + model.getPanier().getQtySmall(position));
        holder.getQtyLargeView().setText("" + model.getPanier().getQtyLarge(position));
        if ((model.getSelected() != null)
                && (holder.getAdapterPosition() == model.getSelected())) {
            holder.itemView.setActivated(true);
        }
        else {
            holder.itemView.setActivated(false);
        }

    }

    @Override
    public int getItemCount() {
        return model.getPizzas().length;
    }
    public void select(Integer position){
        model.setSelected(position);
        Log.d("PIZZA", "Item " + position + " selected.");
        Integer old = model.getSelected();
        if (old != null) {
            notifyItemChanged(old);
        }
        model.setSelected(position);
        notifyItemChanged(position);
    }

}