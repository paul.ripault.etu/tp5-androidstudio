package fr.ulille.iutinfo.jmplace.pizzadroid.pizzaland;

public class Panier {

    private final Pizza[] pizzas ;
    private final int[] qtySmall ;
    private final int[] qtyLarge ;

    public Panier(Pizza[] pizzas) {
        this.pizzas = pizzas;
        this.qtySmall = new int[pizzas.length];
        this.qtyLarge = new int[pizzas.length];
        // Initialisations a fin de test: A supprimer en production
        this.qtyLarge[1] = 2;
        this.qtySmall[2] = 5;
    }

    public int getQtySmall(int position) {
        return this.qtySmall[position];
    }

    public int getQtyLarge(int position) {
        return this.qtyLarge[position];
    }

    public double getAmount() {
        double amount = 0.0;
        for (int i = 0; i < pizzas.length; i++) {
            amount += qtySmall[i] * pizzas[i].getPrix_petite();
            amount += qtyLarge[i] * pizzas[i].getPrix_grande();
        }
        return amount;
    }
}
